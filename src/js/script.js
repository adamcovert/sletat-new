$(document).ready(() => {

  /**
   * Open ad close mobile menu
   */

  $('.page-header__burger').on('click', function () {
    $('.mobile-menu').addClass('mobile-menu--is-active');
  });

  $('.mobile-menu__close').on('click', function () {
    $('.mobile-menu').removeClass('mobile-menu--is-active');
  });



  /**
   * Swiper sliders
   */

  var swiper = new Swiper('.promo__slider', {
    speed: 500,
    pagination: {
      el: '.promo__slider-pagination',
      type: 'bullets',
    },
  });

  var swiper = new Swiper('.partners__slider', {
    speed: 500,
    loop: true,
    autoplay: {
      delay: 5000,
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      420: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      576: {
        slidesPerView: 4,
        spaceBetween: 40
      },
      768: {
        slidesPerView: 5,
        spaceBetween: 40
      },
      992: {
        slidesPerView: 6,
        spaceBetween: 40
      }
    }
  });

  var swiper = new Swiper('.offers__slider', {
    navigation: {
      nextEl: '.offers__slider-nav .slider-nav__btn--next',
      prevEl: '.offers__slider-nav .slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 20
      }
    }
  });

  var swiper = new Swiper('.news__slider', {
    navigation: {
      nextEl: '.news__slider-nav .slider-nav__btn--next',
      prevEl: '.news__slider-nav .slider-nav__btn--prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    }
  });



  /**
   * Accordion
   */

  var disclosure = new Houdini('[data-houdini-group="pirates"]', {
    isAccordion: true,
    collapseOthers: true
  });



  /**
   * Tabs
   */

  var tabs = new Tabby('[data-tabs]');
  var tabs = new Tabby('[data-tabs="feedback"]');
});